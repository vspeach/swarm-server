package main

import (
	"apis_generator/models"
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/template/html/v2"
	"gopkg.in/yaml.v3"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
)

const (
	ApisFileName      = "apis.yaml"
	ApisProxyFileName = "apis_proxy.yaml"
)

func BuildProxy(request []models.InputRequest) *models.ApisProxyYaml {
	var apisProxyYaml models.ApisProxyYaml
	apisProxyYaml.HttpApis = make([]models.APIProxy, len(request))
	for i, v := range request {
		apisProxyYaml.HttpApis[i] = models.APIProxy{
			Title:         v.Title,
			Description:   v.Description,
			API:           v.HandlerName,
			ProxyAuthType: v.ProxyAuthType,
			Resource: yaml.Node{
				Kind:        yaml.ScalarNode,
				Style:       yaml.SingleQuotedStyle,
				Value:       v.Resource.String(),
				LineComment: v.ResourceComment,
			},
			Queries: models.Query{},
			RawBody: models.Body{},
			Do:      "configs name",
		}
	}
	return &apisProxyYaml
}

func BuildApis(request []models.InputRequest) *models.ApisYaml {
	var apisYaml models.ApisYaml
	apisYaml.HttpApis = make([]models.API, len(request))

	var doFormat = "DB|wb_school_pg|%s|%s"

	for i, v := range request {
		if v.SPParams == "" {
			doFormat = "DB|wb_school_pg|%s"
		}

		apisYaml.HttpApis[i] = models.API{
			Title:       v.Title,
			Description: v.Description,
			API:         fmt.Sprintf("%s|%s", v.RequestType, v.HandlerName),
			AuthType:    v.AuthType,
			Queries:     models.Query{},
			RawBody:     models.Body{},
			Do:          fmt.Sprintf(doFormat, v.SPName, v.SPParams),
		}
	}
	return &apisYaml
}

func BuildApisWithProxy() {

}

func WriteApiWithProxy(request []models.InputRequest) {
	apis, err := os.Create(ApisFileName)
	if err != nil {
		panic(err)
	}

	apisProxy, err := os.Create(ApisProxyFileName)
	if err != nil {
		panic(err)
	}

	apisYamlBytes, err := yaml.Marshal(BuildApis(request))
	if err != nil {
		panic(err)
	}

	apisProxyYamlBytes, err := yaml.Marshal(BuildProxy(request))
	if err != nil {
		panic(err)
	}

	_, err = apis.Write(apisYamlBytes)
	if err != nil {
		panic(err)
	}
	_, err = apisProxy.Write(apisProxyYamlBytes)
	if err != nil {
		panic(err)
	}

}

func ReadApisYaml() {
	var popo models.ApisYaml
	open, err := os.Open("examples/apis.yaml")
	if err != nil {
		panic(err)
	}

	all, err := io.ReadAll(open)
	if err != nil {
		panic(err)
	}

	err = yaml.Unmarshal(all, &popo)
	if err != nil {
		log.Panicf("%+v", err)
	}

	log.Printf("succesfully %+v", popo)
}

func checkApisYamlExist() bool {
	_, err := os.Open(ApisFileName)
	return err == nil
}

func ServicesReader() ([]string, error) {
	var names []string
	dirs, err := os.ReadDir("../")
	if err != nil {
		return nil, err
	}

	for i := range dirs {
		if dirs[i].IsDir() && !strings.Contains(dirs[i].Name(), ".") {
			names = append(names, dirs[i].Name())

		}
	}
	return names, err
}

func main() {
	app := fiber.New(fiber.Config{
		Views: html.New("./ui", ".html"),
	})

	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowHeaders: "Origin, Content-Type, Accept",
	}))

	app.Post("/generate", func(c *fiber.Ctx) error {
		var request []models.InputRequest
		err := json.Unmarshal(c.Body(), &request)
		if err != nil {
			return err
		}
		WriteApiWithProxy(request)
		err = c.SendFile(ApisFileName)
		if err != nil {
			return err
		}
		err = c.SendFile(ApisProxyFileName)
		if err != nil {
			return err
		}
		return c.SendStatus(http.StatusOK)
	})

	app.Get("/", func(c *fiber.Ctx) error {
		return c.Render("index", nil)
	})

	log.Fatal(app.Listen(":8080"))
}
