package models

type ApisYaml struct {
	Title       string `yaml:"title"`
	Description string `yaml:"description"`
	HttpApis    []API  `yaml:"httpApis"`
}

type API struct {
	Title       string `yaml:"title" json:"title,omitempty"`
	Description string `yaml:"description,omitempty" json:"description,omitempty"`
	API         string `yaml:"api" json:"api,omitempty"`
	AuthType    string `yaml:"auth_type" json:"auth_type,omitempty"`
	Queries     Query  `yaml:"query,omitempty" json:"queries"`
	RawBody     Body   `yaml:"body,omitempty" json:"raw_body"`
	Do          string `yaml:"do" json:"do,omitempty"`
}
