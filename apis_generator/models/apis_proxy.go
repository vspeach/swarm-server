package models

import (
	"gopkg.in/yaml.v3"
)

type ApisProxyYaml struct {
	Title       string     `yaml:"title"`
	Description string     `yaml:"description"`
	HttpApis    []APIProxy `yaml:"httpApis"`
}

type APIProxy struct {
	Title         string    `yaml:"title" json:"title,omitempty"`
	Description   string    `yaml:"description,omitempty" json:"description,omitempty"`
	API           string    `yaml:"api" json:"api,omitempty"`
	ProxyAuthType string    `yaml:"auth_type" json:"auth_type,omitempty"`
	Resource      yaml.Node `json:"resource,omitempty"`
	Queries       Query     `yaml:"query,omitempty" json:"queries"`
	RawBody       Body      `yaml:"body,omitempty" json:"raw_body"`
	Do            string    `yaml:"do" json:"do,omitempty"`
}
