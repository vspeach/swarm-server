package models

import "github.com/google/uuid"

type InputRequest struct {
	Title           string    `yaml:"title"`
	Description     string    `yaml:"description"`
	ProxyAuthType   string    `json:"proxy_auth_type,omitempty"`  // proxy
	ResourceComment string    `json:"resource_comment,omitempty"` // proxy
	Resource        uuid.UUID `json:"resource,omitempty"`         // proxy
	NeedProxy       string    `json:"need_proxy"`
	AuthType        string    `json:"auth_type,omitempty"`
	RequestType     string    `json:"request_type,omitempty"`
	HandlerName     string    `json:"handler_name,omitempty"`
	SPName          string    `json:"sp_name,omitempty"`
	SPParams        string    `json:"sp_params"`
}

type Query struct {
	TypeName string  `yaml:"type_name"`
	Fields   []Field `yaml:"fields"`
}

type Field struct {
	Name    string `yaml:"Name"`
	Type    string `yaml:"Type"`
	Tag     string `yaml:"Tag"`
	IsSlice bool   `yaml:"IsSlice,omitempty"`
}

type Body struct {
	StructureName string `yaml:"structure_name"`
}

type Schema struct {
	TypeName string  `yaml:"type_name"`
	Fields   []Field `yaml:"fields"`
}
